﻿var uriInfo = 'api/auth';
var uri = 'api/user';

function formatInfo(item) {
    return 'Hello, ' + item.Name + '! Your token is ' + item.Token.value;
}

$(document).ready(function () {
    // Send an AJAX request
    $.getJSON(uriInfo)
        .done(function (data) {
            $('#info').text(formatInfo(data));
        });

    $.getJSON(uri)
        .done(function (data) {
            // On success, 'data' contains a list of products.
            $.each(data, function (key, item) {
                // Add a list item for the product.
                $('<li>', { text: formatItem(item) }).appendTo($('#users'));
            });
        });
});

//function formatItem(item) {
//    return item.Name + ': #' + item.RegisterDate;
//}

function formatItem(item) {
    return 'Id: ' + item.Id + '  ---> Email: #' + item.Email + '  ---> UserName: ' + item.UserName + '  ---> LastName: ' + item.LastName;
}

function find() {
    var id = $('#userId').val();
    $.getJSON(uri + '/' + id)
        .done(function (data) {
            $('#user').text(formatItem(data));
        })
        .fail(function (jqXHR, textStatus, err) {
            $('#user').text('Error: ' + err);
        });
}